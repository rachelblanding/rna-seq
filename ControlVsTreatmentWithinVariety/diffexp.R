##############################################
##### This script shows how to identify ######
#####  expressed genes in RNA-Seq data  ######
##############################################

source("../src/Common.R")

##### Read gene annotations ######
annotations=getGeneDescription()

##### Read counts per gene ######
counts=getCounts()

# read sample information
# each row is a sample
sample_info=getCodeBook()

# check that sample_info row names match 
# counts column names
row.names(sample_info)==names(counts)

# Install EdgeR library if you have not already done so
# see:
# https://bioconductor.org/packages/release/bioc/html/edgeR.html
# for instructions

# load EdgeR library 
library(edgeR)


##### Find differentially expressed genes ######

# As an example, compare Heinz control and heat-treated samples
# Note: to compare different groups, change the two variables
# below
group1_name='H.C'
group2_name='H.S'

# Create a DGEList that contains 
# only two sample types 
group1_indexes=grep(group1_name,names(counts)) 
group2_indexes=grep(group2_name,names(counts))
indexes=c(group1_indexes,group2_indexes)
sample_groups=sample_info$group[indexes]
little_DGEList=DGEList(counts[,indexes],
                       group = sample_groups,
                       remove.zeros = TRUE)

# make a design matrix that indicates which samples
# belong to which groups
# we're using the group means parameterization scheme here
# this is a convenience that ensures the model parameters
# are group means
# note: we are not including replicate as a factor in the model
design=model.matrix(~0+group,data=little_DGEList$samples)
colnames(design)=levels(little_DGEList$samples$group)

# estimate variance parameters needed to fit a 
# model of gene expression
little_DGEList = estimateDisp(little_DGEList,design) 

# fit a generalized linear model to the data
fit = glmFit(little_DGEList, design)

# make a contrast matrix to run a statistical test
# comparing groups
comparison=paste(group2_name,"-",group1_name)
contrast=makeContrasts(comparison,levels=design)

# run the test
lrt=glmTreat(fit,contrast=contrast,lfc=0.1)

# save results to a variable
results=lrt$table

# add a column with false discovery rate to deal with 
# multiple hypothesis testing problem
results$Q=p.adjust(results$PValue,method="fdr")

# add a column with gene name
results$gene=rownames(results)

# groom results - keeping genes with smallest Q
Q=0.6
to_keep=results$Q<Q
results=results[to_keep,c('gene','logFC','Q')]

# combine annotations with DE results
results=merge(results,annotations,by.x = "gene",by.y="gene")

# to sanity check results, add columns with
# average expression data
# fpm - fragments per million
# (probably should use averages from model)
fpm=cpm(counts)
# make a new data frame with average normalized counts
group1_ave=rowMeans(fpm[,grep(group1_name,colnames(fpm))])
group2_ave=rowMeans(fpm[,grep(group2_name,colnames(fpm))])
aves=data.frame(gene=row.names(fpm),
                group1=group1_ave,
                group2=group2_ave)
names(aves)[2:3]=c(group1_name,group2_name)

# merge the averages data frame with results
results=merge(results,aves,by.x="gene",by.y="gene")

# order by log fold-change
o=order(results$logFC,decreasing = TRUE)
results = results[o,]

# save result to a file
results_filename=paste0(group1_name,"to",group2_name,".txt")
write.table(results,file=results_filename,quote=FALSE,
            sep="\t",row.names = FALSE)


##### Explore results ######

# view results - select in the Environment tab
# page up and down to see lists of DE genes
# or keyword search

# examples:
# 17.9 kDa class II heat shock protein
g1="Solyc08g062340.3"
# invertase 5
g2="Solyc09g010080.3"
# view a barplot, color-coded by sample type
sample_colors = getSampleColors()
barplot(fpm[g2,],beside=T,
        main=paste(g,"Expression Profile"),
        col=sample_colors,las=2,ylab="FPM")

# You try it! Pick two samples to compare. How many
# DE genes? 

# hint: replace group1_name and group2_name with 
# different samples






