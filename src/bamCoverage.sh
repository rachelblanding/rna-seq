#!/bin/bash
#PBS -l nodes=1:ppn=2
#PBS -l mem=8gb
#PBS -l walltime=15:00:00
#PBS -q copperhead

cd $PBS_O_WORKDIR

# S,F defined by qsub -v
# S - sample name
# F - file name; should be file.bam

# from https://deeptools.readthedocs.io/en/latest/content/tools/bamCoverage.html#usage-examples-for-rna-seq
opts="--binSize 1 --normalizeUsing CPM"
bamCoverage $opts --filterRNAstrand forward -b $F -o $S.fwd.bw
bamCoverage $opts --filterRNAstrand reverse -b $F -o $S.rev.bw
