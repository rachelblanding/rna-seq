#!/bin/bash

# to run: 
#    qsub-doIt.sh [extension] [script] >jobs.out 2>jobs.err
# to kill:
#    cat jobs.out | xargs qdel 
#
# example)
#
#   qsub-doIt.sh .sam samtools.sh >jobs.out 2>jobs.err
#

EXT=$1
SCRIPT=$2

FS=$(ls *${EXT})
for F in $FS
do
    S=$(basename $F ${EXT})
    CMD="qsub -N $S -o $S.out -e $S.err -vS=$S,F=$F $SCRIPT"
    $CMD
done
